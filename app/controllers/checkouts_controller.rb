class CheckoutsController < ApplicationController
  
  def index
    
  end

  def show
    @checkout = Checkout.new(PriceRule.all)
    params["products"].each do |product_code|
      @checkout.scan(product_code)
    end
  end
  
end

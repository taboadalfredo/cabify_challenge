class PriceRuleType
  
  def self.discount(discount_factor, product_amount, product_price)
    fail NotImplementedError, "A price rule class must be able to calculate a discount!"
  end
  
end

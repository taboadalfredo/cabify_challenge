class Checkout
  def initialize(pricing_rules = {})
    @pricing_rules = pricing_rules
    @products = []
  end
  
  def scan(product_code)
    product = Product.find_by code: product_code
    products << product if product
  end
  
  def total
    discount = 0
    
    pricing_rules.each do |a_rule|
      discount += a_rule.calculate_discount(products)
    end
    
    (products.sum {|each| each.price}) - discount
  end
  
  def products
    @products ||= []
  end
  
  def pricing_rules
    @pricing_rules ||= {}
  end
end

class PriceRule < ApplicationRecord
  belongs_to :product
  validates :name, :discount_factor, :discount, presence: true
  
  def calculate_discount(products)
    product_amount = (products.select {|each| each == product}).size
    price_rule_type.discount(discount_factor, product_amount, product.price, discount)
  end
  
  def price_rule_type
    @price_rule_type ||= read_attribute('price_rule_type').constantize
  end
end

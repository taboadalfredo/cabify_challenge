class DiscountForAmount < PriceRuleType
  
  def self.discount(discount_factor, product_amount, product_price, discount)
    product_amount >= discount_factor ? ((product_amount * product_price) * discount) : 0
  end
  
end

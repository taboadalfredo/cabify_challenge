# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Product.create(name: 'Cabify Voucher', code: 'VOUCHER', price: 5.00)
Product.create(name: 'Cabify T-Shirt', code: 'TSHIRT', price: 20.00)
Product.create(name: 'Cafify Coffee Mug', code: 'MUG', price: 7.50)

PriceRule.create(name: 'twoforone', price_rule_type: 'BuyNGetOneFree', discount_factor: 2, discount: 100.00, product: (Product.find_by code: 'VOUCHER'))
PriceRule.create(name: 'discountforamount', price_rule_type: 'DiscountForAmount', discount_factor: 3, discount: 0.05, product: (Product.find_by code: 'TSHIRT'))
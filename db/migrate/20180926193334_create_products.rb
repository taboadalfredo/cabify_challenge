class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :name, null: false
      t.string :code, null: false
      t.float :price, default: 0.00
      
      t.timestamps
    end
  end
end

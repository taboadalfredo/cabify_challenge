class CreatePriceRule < ActiveRecord::Migration[5.2]
  def change
    create_table :price_rules do |t|
      t.string :name, null: false
      t.string :price_rule_type
      t.integer :discount_factor
      t.float :discount
      
      t.timestamps
    end
    add_reference :price_rules, :product
    add_foreign_key :price_rules, :products
  end
end

require 'test_helper'

class CheckoutTest < ActiveSupport::TestCase
  def setup
    @pricing_rules = [price_rules(:twoforone), price_rules(:discountforamount)]
  end
  
  test "checkout_scan_add_a_product_when_exist" do
    co = Checkout.new
    co.scan("VOUCHER")
    
    assert (co.products.size == 1)
    assert (co.products.first.code == "VOUCHER")
    
    co.scan("DONTEXIST")
    assert (co.products.size == 1)
    assert (co.products.first.code == "VOUCHER")
  end

  test "check_out_total_sum_individual_prices_when_dont_have_pricing_rules" do
    co = Checkout.new()
    co.scan("VOUCHER")
    co.scan("TSHIRT")
    co.scan("MUG")
    
    assert co.total == 32.50
    
    co.scan("VOUCHER")
    assert co.total == 37.50
  end

  test "check_out_total_pricing_rules_dont_apply" do
    co = Checkout.new( @pricing_rules)
    co.scan("VOUCHER")
    co.scan("TSHIRT")
    co.scan("MUG")
    
    assert co.total == 32.50
  end

  test "check_out_total_apply_one_rule" do
    co = Checkout.new(@pricing_rules)
    co.scan("VOUCHER")
    co.scan("TSHIRT")
    co.scan("VOUCHER")
    
    price = co.total
    
    assert price == 25.00
  end

  test "check_out_total_apply_rules" do
    co = Checkout.new(@pricing_rules)
    co.scan("VOUCHER")
    co.scan("TSHIRT")
    co.scan("VOUCHER")
    co.scan("VOUCHER")
    co.scan("MUG")
    co.scan("TSHIRT")
    co.scan("TSHIRT")

    price = co.total
    
    assert price == 74.50
  end
end
require 'test_helper'

class DiscountForAmountTest < ActiveSupport::TestCase
 
  test "you buy less that 3 then the price is the same." do
    assert DiscountForAmount.discount(3, 2, 20.00, 0.05) == 0.00
  end
  
  test "you buy 3 then the price per unit should be less." do
    assert DiscountForAmount.discount(3, 3, 20.00, 0.05) == 3.00
  end
  
  test "you buy more than 3 then the price per unit should be less." do
    assert DiscountForAmount.discount(3, 4, 20.00, 0.05) == 4.00
  end
  
end

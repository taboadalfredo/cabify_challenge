require 'test_helper'

class BuyNGetOneFreeTest < ActiveSupport::TestCase
  
  test "you buy less that 2 then the price is the same." do
    assert BuyNGetOneFree.discount(2, 1, 20.00, 100) == 0.00
  end
  
  test "you buy 3 then you get one free." do
    assert BuyNGetOneFree.discount(3, 3, 20.00, 100) == 20.00
  end
  
  test "you buy 3 but the promo is 2 for 1, then you get one free." do
    assert BuyNGetOneFree.discount(2, 3, 20.00, 100) == 20.00
  end
  
  test "you buy 5 but the promo is 2 for 1, then you get two free." do
    assert BuyNGetOneFree.discount(2, 5, 20.00, 100) == 40.00
  end
  
  test "you buy 3 then you get one free(ignoring the discount percent)." do
    assert BuyNGetOneFree.discount(3, 3, 20.00, 50) == 20.00
  end
  
end

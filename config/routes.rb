Rails.application.routes.draw do
  root 'checkouts#index'
  post  '/show',    to: 'checkouts#show'
end